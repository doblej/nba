/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nba;

/**
 *
 * @author jorge
 */
public class Jugador {
    String codigo, Nombre, Procedencia, Altura, Peso, Posicion, Nombre_equipo;

    public Jugador(String codigo, String Nombre, String Procedencia, String Altura, String Peso, String Posicion, String Nombre_equipo) {
        this.codigo = codigo;
        this.Nombre = Nombre;
        this.Procedencia = Procedencia;
        this.Altura = Altura;
        this.Peso = Peso;
        this.Posicion = Posicion;
        this.Nombre_equipo = Nombre_equipo;
    }
    
    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getProcedencia() {
        return Procedencia;
    }

    public void setProcedencia(String Procedencia) {
        this.Procedencia = Procedencia;
    }

    public String getAltura() {
        return Altura;
    }

    public void setAltura(String Altura) {
        this.Altura = Altura;
    }

    public String getPeso() {
        return Peso;
    }

    public void setPeso(String Peso) {
        this.Peso = Peso;
    }

    public String getPosicion() {
        return Posicion;
    }

    public void setPosicion(String Posicion) {
        this.Posicion = Posicion;
    }

    public String getNombre_equipo() {
        return Nombre_equipo;
    }

    public void setNombre_equipo(String Nombre_equipo) {
        this.Nombre_equipo = Nombre_equipo;
    }
}
