/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nba;

import java.sql.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author jorge
 */
public class FXMLDocumentController implements Initializable {
    public List<String> equipos=new ArrayList<String>();
    public static Connection con;
    public static ResultSet rs;
    @FXML
    private Label label;
    @FXML
    private Button aceptar;
    @FXML
    private Button conexion;
    @FXML
    private TableView<Jugador> tabla;
    @FXML
    private TableColumn<Jugador, String> cod_id;
    @FXML
    private TableColumn<Jugador, String> cod_nombre;
    @FXML
    private TableColumn<Jugador, String> cod_proc;
    @FXML
    private TableColumn<Jugador, String> cod_alt;
    @FXML
    private TableColumn<Jugador, String> cod_pes;
    @FXML
    private TableColumn<Jugador, String> cod_pos;
    @FXML
    private TableColumn<Jugador, String> cod_equ;
    
    ObservableList<Jugador> base=FXCollections.observableArrayList();
    @FXML
    private CheckBox c_equ;
    @FXML
    private CheckBox c_cod;
    @FXML
    private CheckBox c_nom;
    @FXML
    private CheckBox c_pro;
    @FXML
    private CheckBox c_alt;
    @FXML
    private CheckBox c_pes;
    @FXML
    private CheckBox c_pos;
    @FXML
    private ChoiceBox<String> opciones;
    
    public void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        try {
            con = ConexionBBDD.getConection();
            rs=con.createStatement().executeQuery("SELECT * FROM jugadores");
            while(rs.next()){
                base.add(new Jugador(rs.getString("codigo"),rs.getString("Nombre"),rs.getString("Procedencia"),
                        rs.getString("Altura"), rs.getString("Peso"), rs.getString("Posicion"), rs.getString("Nombre_equipo")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        menuDesplegable();
    }
    
    
    @FXML
    public void marcar(ActionEvent event){
        if(c_cod.isSelected()){
            cod_id.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        }else{
            cod_id.setCellValueFactory(new PropertyValueFactory<>(null));
        }
        if(c_equ.isSelected()){
            cod_equ.setCellValueFactory(new PropertyValueFactory<>("Nombre_equipo"));
        }else{
            cod_equ.setCellValueFactory(new PropertyValueFactory<>(null));
        }
        if(c_nom.isSelected()){
            cod_nombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
        }else{
            cod_nombre.setCellValueFactory(new PropertyValueFactory<>(null));
        }
        if(c_pro.isSelected()){
            cod_proc.setCellValueFactory(new PropertyValueFactory<>("Procedencia"));
        }else{
            cod_proc.setCellValueFactory(new PropertyValueFactory<>(null));
        }
        if(c_alt.isSelected()){
            cod_alt.setCellValueFactory(new PropertyValueFactory<>("Altura"));
        }else{
            cod_alt.setCellValueFactory(new PropertyValueFactory<>(null));
        }
        if(c_pes.isSelected()){
            cod_pes.setCellValueFactory(new PropertyValueFactory<>("Peso"));
        }else{
            cod_pes.setCellValueFactory(new PropertyValueFactory<>(null));
        }
        if(c_pos.isSelected()){
            cod_pos.setCellValueFactory(new PropertyValueFactory<>("Posicion"));
        }else{
            cod_pos.setCellValueFactory(new PropertyValueFactory<>(null));
        }
        tabla.refresh();
        tabla.setItems(base);
    }
    
    @FXML
    public void salir(ActionEvent event){
        
        System.exit(0);
    }
    public void menuDesplegable(){
        try {
            rs=con.createStatement().executeQuery("SELECT DISTINCT Nombre_equipo FROM jugadores");
            while(rs.next()){
                opciones.getItems().add(rs.getString("Nombre_equipo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
